<?php
// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

//Load Composer's autoloader
require 'vendor/autoload.php';

$mail = new PHPMailer(true);                              // Passing `true` enables exceptions
try {
    //Server settings
    $mail->SMTPDebug = 2;                                 // Enable verbose debug output
    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = "smtp-vip-farm74.uni5.net"; // Endereço do servidor SMTP
	
	$mail->CharSet = 'UTF-8';
	$mail->SMTPAuth = true; // Usa autenticação SMTP? (opcional)
	$mail->Username = 'front-end@agenciadream.com'; // Usuário do servidor SMTP
	$mail->Password = 'dream134'; // Senha do servidor SMTP
	$mail->From = "front-end@agenciadream.com"; // Seu e-mail
	$mail->FromName = "Agência Dream"; // Seu nome
    $mail->Port = 587;
    $mail->SMTPSecure = 'tls';  
	$mail->AddReplyTo("rafael@agenciadream.com","Rafael - Agência Dream");	
	$mail->AddAddress("willyanfontana@gmail.com",'Willyan Fontana');
	
	

    //Content
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = 'Here is the subject';
    $mail->Body    = 'This is the HTML message body <b>in bold!</b>';
    $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

    $mail->send();
    echo 'Message has been sent';
} catch (Exception $e) {
    echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
}