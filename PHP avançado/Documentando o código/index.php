<?php
/*
*Classe para teste
*
*Esta classe ira pegar um valor x e somara com um valor y;
*
*Função somar é a função que vai realizar a ação de soma
*
*@param $x float Primeiro valor a ser somado
*@param $y float Segundo valor a ser somado
*
*@return float
*
*@package Controle de estoque;
*@author Willyan Fontana <willyanfontana@gmail.com>;
*
*/
class Teste{

   private $nome;

    public function somar($x, $y) {
        return $x + $y;
    }

}


?>