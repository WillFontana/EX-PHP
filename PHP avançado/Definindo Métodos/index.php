<?php

class Usuario {
    //3 tipos de funções:
    
    //Public: será acessivel por fora do nosso objeto,
    //Quando queremos tornar as ações de uma função publica;
    //EX:
    public function trocarUsername($nomeAtual, $novoNome){
        //conectar ao banco de dados;
    }

    //Private: Quando se fazem funções auxiliares,
    //Para realizar ações que serão privadas a usuaarios;
    //EX:
    public function trocarSenha($senhaAtual, $novaSenha){
        //conectar ao banco de dados; function conectarAoBanco();
        //verificar senha atual correspondida; function verificarSenha();
        //trocar senha;
    }

    Private function conectarAoBanco(){
        /////////////////////////////
    }
    Private function verificarSenha(){
        /////////////////////////////
    }

    
    //Protected:
    //Só pode ser acessado por dentro de sua clase ou dentro de uma classe agregada a ele;
    protected function cuidarDaCasa(){
        
    }
}

?>