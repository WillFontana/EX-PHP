<?php
    class Usuario{
        private $nome;
        private $idade;
        private $email;
        private $setor;
        private $qtSetor;

        
            public function getNome(){
                return $this->nome;
            }
            public function setNome($n){
                if (is_string($n)) {
                    $this->nome = $n;
            }
        }
            public function addSetor($str){
                $this->setor[] = $str;
                $this->contarSetores();
            }
            public function getQuantosSetores(){
                //return count($this->comentarios);
                return $this->qtSetor;
            }
            private function contarSetores(){
                $this->qtSetor = count($this->setor);
            }
    
    
    }
    $usuario = new Usuario();
    $usuario->addSetor("ADS");
    $usuario->addSetor("Gastronomia");
    $usuario->addSetor("Gestão");
    $usuario->addSetor("TI");
    $usuario->addSetor("Social");

    echo "Quantidade de setores: ".$usuario->getQuantosSetores();
?>