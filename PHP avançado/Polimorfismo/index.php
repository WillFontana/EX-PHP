<?php
class Animal{
    public function getNome(){
        echo "getNome da classe Animal";
    }
    public function testar(){
        echo "Testado";
    }
}
class Cachorro extends Animal{

    public function getNome(){
        //echo "getNome da classe cachorro";
        $this->testar();

    }

}



$cachorro = new Cachorro();
$cachorro->getNome();

?>