<?php
    class Usuario{
        private $nome;
        private $idade;
        private $email;
        private $setor;

            public function __construct($n, $e, $i, $s){
                $this->setNome($n);
                $this->setEmail($e);
                $this->setIdade($i);
                $this->setSetor($s);
            }
        


            public function getNome(){
                return $this->nome;
            }
            public function setNome($n){
                if (is_string($n)) {
                    $this->nome = $n;
            }
        }



            public function getEmail(){
                return $this->email;
            }
            public function setEmail($e){
                if (is_string($e)){
                    $this->email = $e;
                }
            }



            public function getIdade(){
                return $this->idade;
            }
            public function setIdade($i){
                if (is_string($i)){
                    $this->idade = $i;
                }
            }



            public function getSetor(){
                return $this->setor;
            }
            public function setSetor($s){
                if (is_string($s)){
                    $this->setor = $s;
                }
            }

    }

    $usuario = new Usuario("Willyan Fontana", "willyanfontana@gmail.com", "20 anos", "Analise e Desenvolvimento de Softwares");
    echo "Usuario: ".$usuario->getNome()."<br/>
        E-mail da conta: ".$usuario->getEmail()." <br/>
        Idade do usuario: ".$usuario->getIdade()." <br/>
        Setor de trabalho: ".$usuario->getSetor()." .";

?>