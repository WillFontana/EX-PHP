<?php

$arquivo = "imagem.jpg";
$largura = 200;
$altura = 200;

list($largura_original, $altura_original) = getimagesize($arquivo);

$ratio = $largura_original / $altura_original;

if($largura/$altura > $ratio) {
    $largura = $altura * $ratio;
}else{
    $altura = $largura / $ratio;
}
//echo "Largura Original: ".$largura_original." - Altura Original: ".$altura."</br>";
//echo "Largura: ".$largura." - Altura: ".$altura."</br></br></br></br>";

$imagem_final = imagecreatetruecolor($largura, $altura);
$imagem_origianal = imagecreatefromjpeg($arquivo);

//imagecopyresized(); (pode causar aglomeração de pixels)

imagecopyresampled($imagem_final, $imagem_origianal,
 0, 0, 0, 0,
 $largura, $altura, $largura_original, $altura_original);

//header("Content-Type: image/jpg"); (para exibir a imagem)
//imagejpeg($imagem_final, null, 100);

imagejpeg($imagem_final, "ResizedImg.jpg", 100);
echo "Imagem redimencionada com sucesso!";
?>