<?php

/*
Tudo o que estiver entre esse comando 
sera ignorado pelo php
e será interpretado como comentário
*/

//Serve de explicação do código e sua função
//O código abaixo é o nome do usuário


$nome = "Willyan";
$sobrenome = "Fontana";
$idade = 19;

echo "Meu nome é ".$nome. " ".$sobrenome." e eu tenho ".$idade." anos";
?>
<!----------------------------------------------------------------------------------------->

<?php
/*Tipos de variaveis*/

$nome = "Willyan"; //string
$idade = 19; // inteiro, int, interger
$nota = 9.5; // float
$ligada = true; // boolean. TRUE / FALSE
$grupos = array (1, 2, 3, 4, 5) /*Ex:
$caracteristicas = array ("alegre", "feliz", "alto")*/

?>
<!--------------------------------------------------------------->
<?php
$x = 0;

while($x <= 10) {
    echo "X é igual a ".$x."<br/>";
    $x++;
}

?>

<?php
$x = 0;

echo $x++; // 0
echo $x; // 1
echo ++$x; // 2

?>
<!------------------------------------------------------------------------>
<?php
//o primeiro item dentro do "for" é uma definição antes do loop começar
for($x = 0; $x < 10; $x++) {
    echo "O nosso X é : ".$x."<br/>";
}
?>
<!------------------------------------------------------------------------------->
<?php
$nomes = array( 
    array("nome" => "Willyan", "idade" => 20),
    array("nome" => "Ludmila", "idade" => 15),
    array("nome" => "Reginaldo", "idade" => 19),
    array("nome" => "Heynoan", "idade" => 19)
);
foreach($nomes as $amigo){
    echo "Amigo: " .$amigo["nome"]. " - Idade: " .$amigo["idade"]. "<br/>";

}

?>

<?php
$aluno = array( 
        "nome" => "Willyan",
        "idade" => 19,
        "estado" => "PR",
        "cidade" => "Londrina"
);

 foreach($aluno as $chave => $dados){
     echo $chave. " = " .$dados. "<br/>";
 }
?>

<!----------------------------------------------------------------------------------->
<?php

function somarNumero($x, $y) {
  return $x + $y;
}

$resultado = somarNumero(15, 5);

function mostrarNome(){
    return "Willyan";
}
$nome = mostrarNome();

echo "Meu nome é " .$nome. " e eu tenho " .$resultado. " anos";

?>
<!------------------------------------------------------------------------------------>
<?php

$dataatual = date("d/m/Y \à\s H:i:s");
    echo $dataatual
?>
<!----------------------------------------------------------------------------------------->
<?php

echo abs(10); //retorna o numero absoluto
echo round(10.5);// arredonda o numero de acordo com a cas decimal
echo ceil(5.2);//sempre arredonda o numero para um inteiro maior independente do decimal
echo floor(3.8);//sempre arredonda o numero para um inteiro menor independente do decimal
echo rand(1, 9);// seleciona um numero aleatorio entre os numeros dados

?>
<!--------------------------------------------------------------------------------------------------------------------->
<?php
//ex de sorteio
$lista = array ("Willyan", "Reginaldo", "Ludmila", "Heynoan");

$x = rand(0,3);

echo "Eo sorteado foi : ".$lista[$x];

?>
<!---------------------------------------------------------------------------------------------------------------------->
<?php
//explode pega o caractere de uma string definido e o usa para separar a própria string
$nome = "Willyan Fontana";

$x = explode (" ", $nome);
print_r ($x);

//inplode junta varias strings utilizando uma string definida
$array = array("Willyan", "Fontana");

$nomecmpleto = implode (" ", $array);

echo $nomecmpleto;

//number_format formata um numero decimal da maneira desejada
$x = number_format (8.892738263821, 2 ",", ".");

echo $x;

//str_replace serve para substituir uma string por outra
$texto = "Eu comprarei um ps4!";

$string = str_replace("comprarei", "comprei", $texto);
echo $string;

//strtolower serve para tornara a string em letra minuscula
echo strtolower ("PARA DE GRITAR");

//strtoupper deixa uma string inteira maiuscula
echo strtoupper ("fala mais alto");

//substr seleciona um caractere inicial e final de uma string e só mostra os caracteres contido entre os mesmos
$texto = "Willyan";

$x = substr($texto, 0, 4);
$y = substr($texto, 4, 7);
echo $x."</br>"; 
echo $y;

//ucfirst formata para que a primeira letra seja maiuscula
$nome = "willyan";
echo ucfirst($nome);

//ucwords torna maiuscula cada palavra
$nome = "willyan fontana ";
echo ucwords($nome); 
?>
<!---------------------------------------------------------------------------------------------------------------------------------->
<?php
$array = array (
    "nome" => "Willyan";
    "idade" => 19;
    "cidade" => "Londrina";
    "pais" => "Brasil"
);
$array2 = array_keys($array);
print_r($array2);
?>
<!--------------------------------------------------------------------------------------------------------------------------------->
<?php
$array = array (
    "nome" => "Willyan",
    "idade" => 19,
    "cidade" => "Londrina",
    "pais" => "Brasil"
);
$array2 = array_values($array);
print_r($array2);
?>
<!--------------------------------------------------------------------------------------------------------------------------------->
<?php
$array = array (
    "nome" => "Willyan",
    "idade" => 19,
    "cidade" => "Londrina",
    "pais" => "Brasil"
);

array_pop($array);
print_r($array);
?>
<!------------------------------------------------------------------------------------------------------------------------------------>
<?php
$array = array (
    "nome" => "Willyan",
    "idade" => 19,
    "cidade" => "Londrina",
    "pais" => "Brasil"
);

array_shift($array);
print_r($array);
?>
<!----------------------------------------------------------------------------------------------------------------------------------->
<?php
$array = array (
    "nome" => "Willyan",
    "idade" => 19,
    "cidade" => "Londrina",
    "pais" => "Brasil"
);

asort ($array);
print_r($array);
//ou
$array = array (
    "nome" => "Willyan",
    "idade" => 19,
    "cidade" => "Londrina",
    "pais" => "Brasil"
);

arsort ($array);
print_r($array);
?>
<!--------------------------------------------------------------------------------------------------------------------------->
<?php
$array = array (
   "Willyan",
   "Ludmila",
   "Reginaldo",
   "Heynoan",

);

echo "Total de alunos: ".count($array); 
?>
<!---------------------------------------------------------------------------------------------------------------------------->
<?php
$array = array (
   "Willyan",
   "Ludmila",
   "Reginaldo",
   "Heynoan",

);

if(in_array("Willyan", $array)){
    echo "O vencedor foi Willyan";
}else {
    echo "Não tem vencedores";
}
?>
<!------------------------------------------------------------------------------------------------------------------------------->
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
PADROES DE CRIPTOGRAFIA

IRREVERSIVEL:
<?php
$nome = "Willyan";
$nome2 = md5($nome);

echo "Nome Original: ".$nome."</br>";
echo "Nome criptografado: ".$nome2." .";

?>

REVERSIVEL:
<?php
$nome = "Willyan";
$nome2 = base64_encode($nome);

echo "Nome Original: ".$nome."</br>";
echo "Nome criptografado: ".$nome2." .";
//para decodificar:
$codigo = "V2lsbHlhbg==";

echo "Meu nome original é: ".base64_decode($codigo);
?>
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
<!--------------------------------------------------------------------------------------------------------------------------------->
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
PERIGOS!!!
<?php
$autor = $_POST["autor"]; //' or 1=1------------- '; DROP TABLE posts;

$sql =  "SELECT * FROM posts  WHERE autor = '' '; DROP TABLE posts;"

//PARA RESOLVER:
$autor = addslashes($_POST["autor"]);

$sql =  "SELECT * FROM posts  WHERE autor = 'Willyan\'Fontana'";
?>
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
<!----------------------------------------------------------------------------------------------------------------------------------->
<?php
//gerar uma sessão
session_start();

$_SESSION["teste"] = "Willyan Fontana";

echo "Sessão foi feita...";
?>
<?php
session_start();

echo "Meu nome é: ".$_SESSION["teste"];
?>
<!------------------------------------------------------------------------------------------------------------------------------------->
<?php
//gerar um cookie
setcookie("meuteste", "Ludmila Ferreira", time()+3600);

echo "Cookie setado com sucesso;"
?>
