<?php
session_start();
require 'config.php';

if(isset($_POST['tipo'])){
    $tipo = $_POST['tipo'];
    $valor = str_replace(",", ".", $_POST['valor']);
    $valor = floatVal($valor);

	$sql = $pdo->prepare("INSERT INTO historico (id_conta, tipo, valor, data_operacao) VALUES (:id_conta, :tipo, :valor, NOW())");    $sql->bindValue(":id_conta", $_SESSION['banco']);
    $sql->bindValue(":tipo", $tipo);
    $sql->bindValue(":valor", $valor);
    $sql->execute();



    if($tipo == '0'){
        //depósito
        $sql = $pdo->prepare("UPDATE contas SET saldo = saldo + :valor WHERE id = :id");
        $sql->bindValue(":valor", $valor);
        $sql->bindValue(":id", $_SESSION['banco']);
        $sql->execute();
    }else {
        //saque
        $sql = $pdo->prepare("UPDATE contas SET saldo = saldo - :valor WHERE id = :id");
        $sql->bindValue(":valor", $valor);
        $sql->bindValue(":id", $_SESSION['banco']);
        $sql->execute();
    }
    
    header("Location: index.php");
    exit;

}
?>
<!DOCTYPE html>
<html>
<head>
    <title>Log In</title>
</head>
<body>
    <form method="POST">
       Tipo de operação:<br/>
       <select name="tipo">
            <option value ="0">Depósito</option>
            <option value ="1">Saque</option>
       </select><br/><br/>

       Valor:<br/>
       <input type="text" name="valor" pattern="[0-9.,]{1,}" /><br/><br/>

       <input type="submit" value="Registrar Operação" /><br/><br/>
    </form>
</body>
</html>