<?php
require 'config.php';

$reservas = new Reservas($pdo);
$carros = new Carros($pdo);
?>

<h1>Reservas:</h1>
<?php
$lista = $reservas->getReservas();

foreach($lista as $item){
$data1 = date('d/m/Y', strtotime($item['data_inicio']));
$data2 = date('d/m/Y', strtotime($item['data_entrega']));
    echo $item['reservante'].' reservou o modelo: '.$item['id_carro'].' entre os dias '.$data1.' e '.$data2.' .<br/>';
}
?>