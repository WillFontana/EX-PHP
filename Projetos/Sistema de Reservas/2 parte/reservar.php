<?php
require 'config.php';

$reservas = new Reservas($pdo);
$carros = new Carros($pdo);

if(!empty($_POST['carro'])) {
	$carro = addslashes($_POST['carro']);
	$data_locacao = addslashes($_POST['data_locacao']);
	$data_entrega = addslashes($_POST['data_entrega']);
	$locador = addslashes($_POST['locador']);

	if($reservas->verificarDisponibilidade($carro, $data_locacao, $data_entrega)) {
		$reservas->reservar($carro, $data_locacao, $data_entrega, $locador);
		header("Location: index.php");
		exit;
	} else {
		echo "Não há modelos disponiveis desse carro para locação neste período!";
	}


}




?>
<h1>Adicionar Reserva</h1>

<form method="POST">
	Carro:<br/>
	<select name="carro">
		<?php
		$lista = $carros->getCarros();

		foreach($lista as $carro):
			?>
			<option value="<?php echo $carro['id']; ?>"><?php echo $carro['nome']; ?></option>
			<?php
		endforeach;
		?>
	</select><br/><br/>

	Data da locação:<br/>
	<input type="date" name="data_locacao" /><br/><br/>

	Data da devolução:<br/>
	<input type="date" name="data_entrega" /><br/><br/>

	Nome do locador:<br/>
	<input type="text" name="locador" /><br/><br/>

	<input type="submit" value="Reservar" />
</form>