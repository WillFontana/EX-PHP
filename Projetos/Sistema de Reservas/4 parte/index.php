<?php
require 'config.php';

$reservas = new Reservas($pdo);
$carros = new Carros($pdo);

?>

<h1>Reservas:</h1>

<h3><a href="reservar.php" >Adicionar Reserva!</a><br/></h3>

<form method="GET">
    <select name="ano">
        <?php for($q=date('Y');$q>=2000;$q--): ?>
        <option><?php echo $q; ?></option>
        <?php endfor; ?>
    </select>
    <select name="mes">
        <option>01</option>
        <option>02</option>
        <option>03</option>
        <option>04</option>
        <option>05</option>
        <option>06</option>
        <option>07</option>
        <option>08</option>
        <option>09</option>
        <option>10</option>
        <option>11</option>
        <option>12</option>
    </select>
    <input type="submit" value="Procurar">
</form>


<?php

if (empty($_GET['ano'])) {
    exit;
}

$data = $_GET['ano'].'-'.$_GET['mes'];
$dia1 = date('w', strtotime($data.'-01'));
$dias = date('t', strtotime($data));
$linhas = ceil(($dia1+$dias) / 7);
$dia1 = -$dia1;
$data_inicio = date('Y-m-d', strtotime($dia1.' days', strtotime($data)));
$data_fim = date('Y-m-d', strtotime(( ($dia1 + ($linhas*7) - 1) ).' days', strtotime($data)));



$lista = $reservas->getReservas($data_inicio, $data_fim);
/*foreach($lista as $item){
$data1 = date('d/m/Y', strtotime($item['data_locacao']));
$data2 = date('d/m/Y', strtotime($item['data_entrega']));

echo $item['locador'].' reservou o modelo '.$item['nome'].' entre os dias '.$data1.' e '.$data2.' .<br/>';
}*/
?>
<hr/>
<?php
require 'calendario.php';
?>