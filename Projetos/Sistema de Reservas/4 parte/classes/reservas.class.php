<?php
class Reservas {

	private $pdo;

	public function __construct($pdo) {
		$this->pdo = $pdo;
	}

	public function getReservas($data_inicio, $data_fim) {
		$array = array();

		$sql = "SELECT * FROM reservas INNER JOIN carros ON carros.id = reservas.id_carro WHERE ( NOT ( data_locacao > :data_entrega OR data_entrega < :data_locacao ) )";
		$sql = $this->pdo->prepare($sql);
		$sql->bindValue(":data_locacao", $data_inicio);
		$sql->bindValue(":data_entrega", $data_fim );
		$sql->execute();

		if($sql->rowCount() > 0) {
			$array = $sql->fetchAll();
		}

		return $array;
	}

	public function verificarDisponibilidade($carro, $data_locacao, $data_entrega) {

		$sql = "SELECT
		*
		FROM reservas
		WHERE
		id_carro = :carro AND
		( NOT ( data_locacao > :data_entrega OR data_entrega < :data_locacao ) )";
		$sql = $this->pdo->prepare($sql);
		$sql->bindValue(":carro", $carro);
		$sql->bindValue(":data_locacao", $data_locacao);
		$sql->bindValue(":data_entrega", $data_entrega);
		$sql->execute();

		if($sql->rowCount() > 0) {
			return false;
		} else {
			return true;
		}

	}

	public function reservar($carro, $data_locacao, $data_entrega, $locador) {
		$sql = "INSERT INTO reservas (id_carro, data_locacao, data_entrega, locador) VALUES (:carro, :data_locacao, :data_entrega, :locador)";
		$sql = $this->pdo->prepare($sql);
		$sql->bindValue(":carro", $carro);
		$sql->bindValue(":data_locacao", $data_locacao);
		$sql->bindValue(":data_entrega", $data_entrega);
		$sql->bindValue(":locador", $locador);
		$sql->execute();
	}

}
?>