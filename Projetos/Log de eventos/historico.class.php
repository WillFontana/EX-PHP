<?php
class Historico {
    
    private $pdo;

    public function __construct() {
        $this->pdo = new PDO("mysql:dbname=logeventos;host=localhost", "root", "");

    }

    public function registrar($action) {

        $ip = $_SERVER['REMOTE_ADDR'];

        $sql = "INSERT INTO historico SET ip = :ip, dateAction = NOW(),
        action = :action";
        $sql = $this->pdo->prepare($sql);
        $sql->bindValue(":ip", $ip);
        $sql->bindValue(":action", $action);
        $sql->execute();
    }

}
?>