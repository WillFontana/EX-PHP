<?php
require 'config.php';
require 'acesso.php';

$sql = "SELECT * FROM acessos WHERE hora > :hora GROUP BY ip";
$sql = $pdo->prepare($sql);
$sql->bindValue(":hora", date('H:i:s', strtotime("-5 minutes")));
$sql->execute();
$contagem = $sql->rowCount();

echo "Pessoas online: ".$contagem;

?>