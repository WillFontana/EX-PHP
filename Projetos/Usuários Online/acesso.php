<?php
$ip = $_SERVER['REMOTE_ADDR'];
$hora = date('H:i:s');

#echo "IP: ".$ip." - ".$hora;

$sql = $pdo->prepare("INSERT INTO acessos SET ip = :ip, hora = :hora");
$sql->bindValue(":ip", $ip);
$sql->bindValue(":hora", $hora);
$sql->execute();
$sql = $pdo->prepare("DELETE FROM acessos WHERE hora < :hora");
$sql->bindValue(":hora", date('H:i:s', strtotime("-5 minutes")));
$sql->execute();

?>