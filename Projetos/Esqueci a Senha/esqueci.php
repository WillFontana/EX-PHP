<?php
require 'config.php';
if(!empty($_POST['email'])){

    $email = $_POST['email'];

    $sql = "SELECT * FROM usuarios WHERE email = :email";
    $sql = $pdo->prepare($sql);
    $sql->bindValue(":email", $email);
    $sql->execute();

    if($sql->rowCount() > 0){

        $sql = $sql->fetch();
        $id = $sql['id'];

        $token = md5(time().rand(0, 99).rand(0, 99));


        $sql = "INSERT INTO usuariostoken (id_usuario, hash, expired) VALUES (:id_usuario, :hash, :expired)";
        $sql = $pdo->prepare($sql);
        $sql->bindValue(":id_usuario", $id);
        $sql->bindValue(":hash", $token);
        $sql->bindValue(":expired", date('Y-m-d H:i', strtotime('+2 months')));
        $sql->execute();

        $link = "http://localhost/projetoh/redefinir.php?token=".$token;


        $mensagem = "Acesse o link para redefinir sua senha:<br/>".$link;

        $assunto = "Redefinição de senha";

        $headers = 'From: willyanfontana@gmail.com'."\r\n".'X-Mailer: PHP/'.phpversion();
        #mail($email, $assunto, $mensagem, $headers);

        echo $mensagem;
        exit;
    }
}

?>


<form method="POST">

Email para a recuperação de senha:<br/>
<input type="email" name="email" /><br/><br/>

<input type="submit" value="Recuperar senha" /><br/><br/>
</form>