<?php
session_start();
require 'config.php' ;

if(isset($_POST['agencia']) && !empty($_POST)){
    $agencia = addslashes($_POST['agencia']);
    $conta = addslashes($_POST['conta']);
    $senha = addslashes($_POST['senha']);

    $sql = $pdo->prepare("SELECT * FROM contas WHERE agencia = :agencia AND conta = :conta AND senha = :senha");
    $sql->bindValue(":agencia", $agencia);
    $sql->bindValue(":conta", $conta);
    $sql->bindValue(":senha", md5($senha));
    $sql->execute(); 

    if($sql->rowCount() > 0){
        $sql = $sql->fetch();

        $_SESSION['banco'] = $sql['id'];
        header("Location: index.php");
        exit;
    }


}
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Loh In</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="assets/css/bootstrap.css">
</head>

<body>
    <div class="container">
        <header class="topo"></header>
        <div class="row justify-contents-center">
            <div class="col-sm-5"></div>
            <div class="col-sm-6">
                <h1 class="title">Log In</h1>
            </div>
        </div>
        <div class="row mt">
            <div class="col-sm-1"></div>
            <div class="col-sm-7">
                <form method="POST">
                    <div class="form-group">
                        <label for="agencia" class="labelop">Agência:</label>
                        <input type="number" name="agencia" class="form-control" placeholder="Agência" />
                    </div>
            </div>
        </div>
        <div class="row mt">
            <div class="col-sm-1"></div>
            <div class="col-sm-7">
                <div class="form-group">
                    <label for="conta" class="labelop">Conta:</label>
                    <input type="number" name="conta" class="form-control" placeholder="Conta" />
                </div>
            </div>
        </div>
        <div class="row mt">
            <div class="col-sm-1"></div>
            <div class="col-sm-7">
                <div class="form-group">
                    <label for="senha" class="labelop">Senha:</label>
                    <input type="password" name="senha" class="form-control" placeholder="Senha" />
                </div>
            </div>
        </div>
        <div class="row mt">
            <div class="col-sm-7"></div>
            <div class="col-sm-4">
                <input type="submit" class="btn btn-bank" value="Log In" style="margin:20px;" />
            </div>
        </div>
        </form>











    </div>

    <script type="text/javascript" src="assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.bundle.min.js"></script>
</body>

</html>