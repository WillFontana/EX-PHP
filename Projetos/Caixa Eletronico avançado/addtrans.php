<?php
session_start();
require 'config.php';

if(isset($_POST['tipo'])){
    $tipo = $_POST['tipo'];
    $valor = str_replace(",", ".", $_POST['valor']);
    $valor = floatVal($valor);

	$sql = $pdo->prepare("INSERT INTO historico (id_conta, tipo, valor, data_operacao) VALUES (:id_conta, :tipo, :valor, NOW())");    $sql->bindValue(":id_conta", $_SESSION['banco']);
    $sql->bindValue(":tipo", $tipo);
    $sql->bindValue(":valor", $valor);
    $sql->execute();



    if($tipo == '0'){
        //depósito
        $sql = $pdo->prepare("UPDATE contas SET saldo = saldo + :valor WHERE id = :id");
        $sql->bindValue(":valor", $valor);
        $sql->bindValue(":id", $_SESSION['banco']);
        $sql->execute();
    }else {
        //saque
        $sql = $pdo->prepare("UPDATE contas SET saldo = saldo - :valor WHERE id = :id");
        $sql->bindValue(":valor", $valor);
        $sql->bindValue(":id", $_SESSION['banco']);
        $sql->execute();
    }
    
    header("Location: index.php");
    exit;

}
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Adicionar transação</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="assets/css/bootstrap.css">
</head>

<body>
    <div class="container" style="margin-top:200px;">
        <header class="topo"></header>
        <div class="row justify-content-center">
            <div class="col-sm-2"></div>
            <div class="col-sm-6">
                <h1 class="title">Adicionar transação:</h1>
            </div>
        </div>
        <div class="row mt">
            <div class="col-sm-1"></div>
            <div class="col-sm-6">
                <form method="POST">
                    <div class="form-group">
                        <label for="Operação" class="labelop">Tipo de operação:</label>
                        <select name="tipo" class="btn btn-bank btn-lg">
                            <option value="0">Depósito</option>
                            <option value="1">Saque</option>
                        </select>
                    </div>
            </div>
        </div>
        <div class="row mt">
            <div class="col-sm-1"></div>
            <div class="col-sm-6">
                    <div class="form-group">
                        <label for="Valor:" class="labelop">Valor:</label>
                        <input type="number" name="valor" pattern="[0-9.,]{1,}" class="form-control"
                            placeholder="Valor" />
                    </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <input type="submit" class="btn btn-bank" value="Registrar Operação" style="margin-top:20px;" />
                </div>
            </div>
        </div>
    </form>


    </div>
    <script type="text/javascript" src="assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.bundle.min.js"></script>
</body>

</html>