<?php
session_start();
require'config.php';

if(isset($_SESSION['banco']) && !empty($_SESSION['banco'])){
    $id = $_SESSION['banco'];

    $sql = $pdo->prepare("SELECT * FROM contas WHERE id = :id");
    $sql->bindValue(":id", $id);
    $sql->execute();

    if($sql->rowCount() > 0){
        $info = $sql->fetch();
    } else {
        header("Location: login.php");
        exit;
    }

} else{
    header("Location: login.php");
    exit;
}


?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Banco X</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="assets/css/bootstrap.css">
</head>

<body>
    <div class="container">
        <header class="topo"></header>
        <div class="row justify-content-center">
            <div class="col-sm-3"></div>
            <div class="col-sm-6">
                <h1 class="title">Banco X</h1>
            </div>
        </div>

        <div class="row mt">
            <div class="col-sm-1"></div>
            <div class="col-sm-4">
                <h2 class="title">Titular:</h2>
            </div>
            <div class="sol-sm-6">
            <span class='echo'><?php echo $info['titular'];?></span>
            </div>
            <div class="col-sm-1"></div>
        </div>
        <div class="row mt">
            <div class="col-sm-1"></div>
            <div class="col-sm-4">
                <h2 class="title">Agência:</h2>
            </div>
            <div class="sol-sm-6">
                <span class='echo'><?php echo $info['agencia'];?> </span>
                
                </div>
            <div class="col-sm-1"></div>
        </div>
        <div class="row mt">
            <div class="col-sm-1"></div>
            <div class="col-sm-4">
                <h2 class="title">Saldo disponível:</h2>
            </div>
            <div class="sol-sm-6">
            <span class='echo'><?php echo $info['saldo'];?></span>
            </div>
            <div class="col-sm-1"></div>
        </div>
        <div class="table-responsive">
            <table class="table table-bank table-hover">
                <thead>
                    <tr>
                        <th>Data</th>
                        <th>Valor</th>
                    </tr>
                </thead>
                <?php
                    $sql = $pdo->prepare("SELECT * FROM historico WHERE id_conta = :id_conta");
                    $sql->bindValue(":id_conta", $id);
                    $sql->execute();
            
                    if($sql->rowCount() > 0){
                        foreach($sql->fetchAll() as $item) {
                            ?>
                <tbody>
                    <tr>
                        <th><?php echo date('d/m/Y H:i', strtotime($item['data_operacao'])); ?></th>
                        <th>
                            <?php if($item['tipo'] == '0'): ?>
                            <font color="green">R$ <?php echo $item['valor'] ?></font>
                            <?php else: ?>
                            <font color="red">- R$ <?php echo $item['valor'] ?></font>
                            <?php endif; ?>
                        </th>
                    </tr>
                    <?php
            }
        }
        ?>
                </tbody>
            </table>
        </div>

        <div class="row">
            <div class="col-sm-8"></div>
            <div class="col-sm-3">
                <a href="addtrans.php" class="btn btn-bank">
                    Adicionar Transação
                </a>
            </div>
        </div>

        <div class="row mtl">
            <div class="col-sm-1"></div>
            <div class="col-sm-3">
                <a href="sair.php" class="btn btn-bank btn-block">
                    Sair
                </a>
            </div>
        </div>


    </div>
    <script type="text/javascript" src="assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.bundle.min.js"></script>
</body>

</html>