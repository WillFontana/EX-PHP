<?php
session_start();
require 'config.php';
require 'classes/usuarios.class.php';
require 'classes/docs.class.php';

if (!isset($_SESSION['logado'])){
    header("Location: login.php");
    exit;
}

$usuarios = new Usuarios($pdo);
$usuarios->setUsuario($_SESSION['logado']);
if($usuarios->permission("SECRET") == false){
    header("Location: index.php");
    exit;
}
?>

<h1>Menu Avançado</h1>