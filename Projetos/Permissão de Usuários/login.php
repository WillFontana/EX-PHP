<?php
session_start();
require 'config.php';
require 'classes/usuarios.class.php';

if(!empty($_POST['email'])){
    $email = addslashes($_POST['email']);
    $senha = md5($_POST['senha']);

    $usuarios = new Usuarios($pdo);
    if ($usuarios->fazerLogin($email, $senha)) {
        header("Location: index.php");
        exit;
    }else{
        echo "Usuario ou senha incorretos!";
        exit;
    }
}

?>
<h1>Log In</h1>
<form method="POST">
    E-mail:<br>
    <input type="email" name="email" placeholder="email"><br><br>
    Senha:<br>
    <input type="password" name="senha" placeholder="senha"><br><br>
    <input type="submit" value="Log In">
</form>