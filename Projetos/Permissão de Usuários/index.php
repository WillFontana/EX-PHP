<?php
session_start();
require 'config.php';
require 'classes/usuarios.class.php';
require 'classes/docs.class.php';

if (!isset($_SESSION['logado'])){
    header("Location: login.php");
    exit;
}

$usuarios = new Usuarios($pdo);
$usuarios->setUsuario($_SESSION['logado']);
$docs = new Docs($pdo);
$lista = $docs->getDocs();
?>
<h1>Sistema</h1>
<hr/>
<?php if($usuarios->permission('ADD')): ?>
<a href="">Adicionar Documento</a>
<?php endif; ?>
<hr/>
<?php if($usuarios->permission('SECRET')): ?>
<a href="secreto.php">Menu avançado</a>
<?php endif; ?>
<hr/>
<table border="1" width="100%">
    <tr>
        <th>Documento</th>
        <th>Ações</th>
    </tr>
    <?php foreach ($lista as $item): ?>
    <tr>
        <td><?php echo utf8_encode($item['titulo']); ?></td>
        <td>
        <?php if($usuarios->permission('EDIT')): ?>
        <a href="">Editar</a>
        <?php endif; ?>

        <?php if($usuarios->permission('DEL')): ?>
        <a href="">Excluir</a>
        <?php endif; ?>

    </td>
    </tr>
    <?php endforeach; ?>
</table>