<?php
session_start();
require 'config.php';


if (!empty($_POST['nome']) && !empty($_POST['email'])) {
    $nome = addslashes($_POST['nome']);
    $email = addslashes($_POST['email']);
    $id_pai = $_SESSION['mmnlogin'];
    $senha = md5(addslashes($_POST['email']));

    $sql = $pdo->prepare("SELECT * FROM usuarios WHERE email = :email");
    $sql->bindValue(":email", $email);
    $sql->execute();

    if ($sql->rowCount() == 0) {
        $sql = $pdo->prepare("INSERT INTO usuarios SET id_pai = :id_pai, nome = :nome, email = :email, senha = :senha");
        $sql->bindValue(":id_pai", $id_pai);
        $sql->bindValue(":nome", $nome);
        $sql->bindValue(":email", $email);
        $sql->bindValue(":senha", $senha);
        $sql->execute();

        header("Location: index.php");
        exit;
    }else {
        echo "<h1>Usuario já existente no sistema!</h1>";
    }
}


?>
<h1>Cadastre um novo usuario:</h1>
<hr>
<form method="POST">
    Nome do novo usuario:<br/>
    <input type="text" name="nome" /><br/><br/>
    Email do novo usuario:<br/>
    <input type="email" name="email" /><br/><br/>
    <input type="submit" value="Cadastrar novo usuario" />
</form>