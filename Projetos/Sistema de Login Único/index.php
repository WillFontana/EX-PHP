<?php
session_start();
require 'config.php';
require 'funcoes.php';

if(empty($_SESSION['mmnlogin'])){
    header("Location: login.php");
}

$id = $_SESSION['mmnlogin'];

$sql = $pdo->prepare("SELECT * FROM usuarios WHERE id = :id");
$sql->bindValue(":id", $id);
$sql->execute();

if($sql->rowCount() > 0){
    $sql = $sql->fetch();
    $nome = $sql['nome'];
    $patente = $sql['patente'];
    $id_pai = $sql['id_pai'];

}else{
    header("Location: login.php");
    exit;
}

$lista = listar($id, $limite);

?>
<h1>Sistema de Marketing Multinivel</h1>
<h2>Usuario: <?php echo $nome; ?></h2>
<h2>Patente: <?php echo $patente; ?></h2>
<h2>Cadastrado por: <?php echo $id_pai; ?> </h2>
<a href="cadastrar.php">Cadastrar novo usuario</a><br/><br/>
<a href="sair.php">Sair</a><br/><br/>
<hr/>
<h4>Lista de cadastros:</h4>

<?php exibir($lista); ?>